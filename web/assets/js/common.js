$(document).ready(function(){


    dropdown.init($('.depth1>li'));
});
var dropdown = { //emj header dropdown function
    init : function(e){
        var that = this;
        that.mobile(e);
    },
    mobile : function(e){
        var that = this;
        $('header .hamburger').off('click').on('click',function(event){
            var hamhas = $(this).hasClass('active');
            if(!hamhas){
                that.scrollOff();
                $(this).addClass('active');
                $('header nav.mobile').addClass('active');
            }else{
                that.scrollOn();
                $(this).removeClass('active');
                $('header nav.mobile').removeClass('active');
            }
        });
        $(e).off('click').on('click',function(){
            $(this).siblings().removeClass('active');
            if(that.dropdownHas(e)) $(this).addClass('active');
        });
    },
    dropdownHas : function(e){
        return $(e).children('.dropdown').length;
    },
    scrollOff : function(){
        $('header nav.mobile').addClass('scrollOff').on('scroll touchmove mousewheel', function(e){
            e.preventDefault();
        });
    },
    scrollOn : function(){
        $('header nav.mobile').removeClass('scrollOff').off('scroll touchmove mousewheel');
    }

}
